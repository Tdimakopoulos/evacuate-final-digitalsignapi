/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.exus.digitalsign.db;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author tdim
 */
@Entity
public class MediaFiles implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String filename;
    private String filelocation;
    private String serverurl;
    private String filetype;
    private String status;
    
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MediaFiles)) {
            return false;
        }
        MediaFiles other = (MediaFiles) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "eu.exus.digitalsign.db.MediaFiles[ id=" + id + " ]";
    }

    /**
     * @return the filename
     */
    public String getFilename() {
        return filename;
    }

    /**
     * @param filename the filename to set
     */
    public void setFilename(String filename) {
        this.filename = filename;
    }

    /**
     * @return the filelocation
     */
    public String getFilelocation() {
        return filelocation;
    }

    /**
     * @param filelocation the filelocation to set
     */
    public void setFilelocation(String filelocation) {
        this.filelocation = filelocation;
    }

    /**
     * @return the serverurl
     */
    public String getServerurl() {
        return serverurl;
    }

    /**
     * @param serverurl the serverurl to set
     */
    public void setServerurl(String serverurl) {
        this.serverurl = serverurl;
    }

    /**
     * @return the filetype
     */
    public String getFiletype() {
        return filetype;
    }

    /**
     * @param filetype the filetype to set
     */
    public void setFiletype(String filetype) {
        this.filetype = filetype;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }
    
}

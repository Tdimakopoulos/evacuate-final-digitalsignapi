/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.exus.digitalsign.ws;

import eu.exus.digitalsign.db.DigitalSign;
import eu.exus.interfaces.local.DigitalSignFacadeLocal;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author tdim
 */
@WebService(serviceName = "DigitalSigns")
public class DigitalSigns {
    @EJB
    private DigitalSignFacadeLocal ejbRef;// Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Web Service Operation")

    @WebMethod(operationName = "createDigitalSign")
    @Oneway
    public void createDigitalSign(@WebParam(name = "digitalSign") DigitalSign digitalSign) {
        ejbRef.create(digitalSign);
    }
    
    @WebMethod(operationName = "createDigitalSignWithValues")
    @Oneway
    public void createDigitalSignWithValues(@WebParam(name = "nickname") String nickname,@WebParam(name = "location") String location) {
        DigitalSign digitalSign= new DigitalSign();
        digitalSign.setFileplay(new Long(0));
        digitalSign.setLocation(location);
        digitalSign.setNickname(nickname);
        digitalSign.setStatus("Active");
        digitalSign.setIp("localhost");
        digitalSign.setPort("9090");
        
        ejbRef.create(digitalSign);
    }

    @WebMethod(operationName = "editDigitalSign")
    @Oneway
    public void editDigitalSign(@WebParam(name = "digitalSign") DigitalSign digitalSign) {
        ejbRef.edit(digitalSign);
    }

    @WebMethod(operationName = "removeDigitalSign")
    @Oneway
    public void removeDigitalSign(@WebParam(name = "digitalSign") DigitalSign digitalSign) {
        ejbRef.remove(digitalSign);
    }

    @WebMethod(operationName = "findDigitalSign")
    public DigitalSign findDigitalSign(@WebParam(name = "id") Object id) {
        return ejbRef.find(id);
    }

    @WebMethod(operationName = "findAllDigitalSigns")
    public List<DigitalSign> findAllDigitalSigns() {
        return ejbRef.findAll();
    }

    @WebMethod(operationName = "findDigitalSignsRange")
    public List<DigitalSign> findRangeDigitalSign(@WebParam(name = "range") int[] range) {
        return ejbRef.findRange(range);
    }

    @WebMethod(operationName = "countDigitalSign")
    public int countDigitalSign() {
        return ejbRef.count();
    }
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.exus.digitalsign.ws;

import eu.exus.interfaces.local.MediaFilesFacadeLocal;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author tdim
 */
@WebService(serviceName = "MediaFiles")
public class MediaFiles {
    @EJB
    private MediaFilesFacadeLocal ejbRef;// Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Web Service Operation")

    @WebMethod(operationName = "createMediaFiles")
    @Oneway
    public void createMediaFiles(@WebParam(name = "mediaFiles") eu.exus.digitalsign.db.MediaFiles mediaFiles) {
        ejbRef.create(mediaFiles);
    }
    
    @WebMethod(operationName = "createMediaFilesWithValues")
    @Oneway
    public void createWithValuesMediaFiles(@WebParam(name = "filename") String filename,@WebParam(name = "filetype")String filetype) {
        eu.exus.digitalsign.db.MediaFiles mediaFiles=new eu.exus.digitalsign.db.MediaFiles();
        mediaFiles.setFilelocation("localhost");
        mediaFiles.setFilename(filename);
        mediaFiles.setFiletype(filetype);
        mediaFiles.setServerurl("localhost");
        mediaFiles.setStatus("active");
        ejbRef.create(mediaFiles);
    }

    @WebMethod(operationName = "editMediaFiles")
    @Oneway
    public void editMediaFiles(@WebParam(name = "mediaFiles") eu.exus.digitalsign.db.MediaFiles mediaFiles) {
        ejbRef.edit(mediaFiles);
    }

    @WebMethod(operationName = "removeMediaFiles")
    @Oneway
    public void removeMediaFiles(@WebParam(name = "mediaFiles") eu.exus.digitalsign.db.MediaFiles mediaFiles) {
        ejbRef.remove(mediaFiles);
    }

    @WebMethod(operationName = "findMediaFiles")
    public eu.exus.digitalsign.db.MediaFiles findMediaFiles(@WebParam(name = "id") Object id) {
        return ejbRef.find(id);
    }

    @WebMethod(operationName = "findAllMediaFiles")
    public List<eu.exus.digitalsign.db.MediaFiles> findAllMediaFiles() {
        return ejbRef.findAll();
    }

    @WebMethod(operationName = "findRangeMediaFiles")
    public List<eu.exus.digitalsign.db.MediaFiles> findRangeMediaFiles(@WebParam(name = "range") int[] range) {
        return ejbRef.findRange(range);
    }

    @WebMethod(operationName = "countMediaFiles")
    public int countMediaFiles() {
        return ejbRef.count();
    }
    
}

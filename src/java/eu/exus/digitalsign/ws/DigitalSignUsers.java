/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.exus.digitalsign.ws;

import eu.exus.digitalsign.db.DigitalUsers;
import eu.exus.interfaces.local.DigitalUsersFacadeLocal;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author tdim
 */
@WebService(serviceName = "DigitalSignUsers")
public class DigitalSignUsers {
    @EJB
    private DigitalUsersFacadeLocal ejbRef;// Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Web Service Operation")

    @WebMethod(operationName = "create")
    @Oneway
    public void create(@WebParam(name = "digitalUsers") DigitalUsers digitalUsers) {
        ejbRef.create(digitalUsers);
    }

    @WebMethod(operationName = "edit")
    @Oneway
    public void edit(@WebParam(name = "digitalUsers") DigitalUsers digitalUsers) {
        ejbRef.edit(digitalUsers);
    }

    @WebMethod(operationName = "remove")
    @Oneway
    public void remove(@WebParam(name = "digitalUsers") DigitalUsers digitalUsers) {
        ejbRef.remove(digitalUsers);
    }

    @WebMethod(operationName = "find")
    public DigitalUsers find(@WebParam(name = "id") Object id) {
        return ejbRef.find(id);
    }

    @WebMethod(operationName = "findAll")
    public List<DigitalUsers> findAll() {
        return ejbRef.findAll();
    }

    @WebMethod(operationName = "findRange")
    public List<DigitalUsers> findRange(@WebParam(name = "range") int[] range) {
        return ejbRef.findRange(range);
    }

    @WebMethod(operationName = "count")
    public int count() {
        return ejbRef.count();
    }
    
}

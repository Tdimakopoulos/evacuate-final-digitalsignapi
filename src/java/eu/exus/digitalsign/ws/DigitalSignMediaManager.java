/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.exus.digitalsign.ws;

import eu.exus.digitalsign.db.DigitalSign;
import eu.exus.interfaces.local.DigitalSignFacadeLocal;
import eu.exus.interfaces.local.MediaFilesFacadeLocal;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author tdim
 */
@WebService(serviceName = "DigitalSignMediaManager")
public class DigitalSignMediaManager {

    @EJB
    private DigitalSignFacadeLocal ejbRef;// Add business logic below. (Right-click in editor and choose
    @EJB
    private MediaFilesFacadeLocal ejbRef2;

    @WebMethod(operationName = "PlayMedia")
    public String PlayMedia(@WebParam(name = "SignID") Long MediaID, @WebParam(name = "FileID") Long FileID) {
        DigitalSign pSign = ejbRef.find(MediaID);
        pSign.setFileplay(FileID);
        ejbRef.edit(pSign);

        eu.exus.digitalsign.db.MediaFiles pMedia = ejbRef2.find(FileID);

        PlayMedia(MediaID, pMedia.getFilename(), pMedia.getFiletype());
        return "No Errors";
    }

    @WebMethod(operationName = "StopMedia")
    public String StopMedia(@WebParam(name = "SignID") Long SignID) {
        DigitalSign pSign = ejbRef.find(SignID);
        pSign.setFileplay(new Long(0));
        ejbRef.edit(pSign);
        RemoveAllMedia(SignID);
        return "No Errors";
    }

    private void RemoveAllMedia(Long Signid) {
        File fnew = new File("/var/www/html/evacuate/" + Signid.toString() + ".html");
        try {
            FileWriter f2 = new FileWriter(fnew, false);

            f2.write("<!DOCTYPE html>");
            f2.write("<html>");

            f2.write("<head>");
            f2.write("<title>Evacuate Digital Sign</title>");
            f2.write("<meta http-equiv=\"refresh\" content=\"5\">");
            f2.write("</head>");


            f2.write("<body>");
            f2.write("<div style=\"text-align:center\">");
            //f2.write("<video width=\"800\" height=\"480\" autoplay loop>");
            //f2.write("<source src=\"mov_doorleft.mp4\"type=\"video/mp4\">");
            //f2.write("Yourbrowser doesnot support HTML5 video");
            //f2.write("</video>");
            f2.write("</div>");
            f2.write("</body>");
            f2.write("</html>");
            f2.close();
        } catch (IOException e) {
            System.out.println("Error on Stop all media");
        }

    }

    private void PlayMedia(Long Signid, String filename, String type) {
        File fnew = new File("/var/www/html/evacuate/" + Signid.toString() + ".html");
        try {
            FileWriter f2 = new FileWriter(fnew, false);

            f2.write("<!DOCTYPE html>");
            f2.write("<html>");
            f2.write("<head>");
            f2.write("<title>Evacuate Digital Sign</title>");
            f2.write("<meta http-equiv=\"refresh\" content=\"5\">");
            f2.write("</head>");
            f2.write("<body>");
            f2.write("<div style=\"text-align:center\">");
            if (type.equalsIgnoreCase("video")) {
                f2.write("<video width=\"800\" height=\"480\" autoplay loop>");
                f2.write("<source src=\"" + filename + "\"type=\"video/mp4\">");
                f2.write("Yourbrowser doesnot support HTML5 video");
                f2.write("</video>");
            }
            if (type.equalsIgnoreCase("sound")) {
                f2.write("<audio controls>");
                f2.write("<source src=\"" + filename + "\" type=\"audio/wav\">");

                f2.write("Your browser does not support the audio element.");
                f2.write("</audio>");
            }
            if (type.equalsIgnoreCase("picture")) {
                f2.write("<img src=\"" + filename + "\" alt=\"Evacuate Digital Sign Image\" title=\"Digital Sign Image\"/>");
            }
            f2.write("</div>");
            f2.write("</body>");
            f2.write("</html>");
            f2.close();
        } catch (IOException e) {
            System.out.println("Error on Stop all media");
        }

    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.exus.interfaces.local;

import eu.exus.digitalsign.db.DigitalSign;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author tdim
 */
@Local
public interface DigitalSignFacadeLocal {

    void create(DigitalSign digitalSign);

    void edit(DigitalSign digitalSign);

    void remove(DigitalSign digitalSign);

    DigitalSign find(Object id);

    List<DigitalSign> findAll();

    List<DigitalSign> findRange(int[] range);

    int count();
    
}

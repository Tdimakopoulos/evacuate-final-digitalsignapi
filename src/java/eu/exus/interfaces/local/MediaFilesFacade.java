/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.exus.interfaces.local;

import eu.exus.digitalsign.db.MediaFiles;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author tdim
 */
@Stateless
public class MediaFilesFacade extends AbstractFacade<MediaFiles> implements MediaFilesFacadeLocal {
    @PersistenceContext(unitName = "DigitalSignAPIPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public MediaFilesFacade() {
        super(MediaFiles.class);
    }
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.exus.interfaces.local;

import eu.exus.digitalsign.db.MediaFiles;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author tdim
 */
@Local
public interface MediaFilesFacadeLocal {

    void create(MediaFiles mediaFiles);

    void edit(MediaFiles mediaFiles);

    void remove(MediaFiles mediaFiles);

    MediaFiles find(Object id);

    List<MediaFiles> findAll();

    List<MediaFiles> findRange(int[] range);

    int count();
    
}

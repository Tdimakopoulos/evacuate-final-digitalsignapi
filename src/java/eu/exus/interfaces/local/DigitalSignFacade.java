/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.exus.interfaces.local;

import eu.exus.digitalsign.db.DigitalSign;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author tdim
 */
@Stateless
public class DigitalSignFacade extends AbstractFacade<DigitalSign> implements DigitalSignFacadeLocal {
    @PersistenceContext(unitName = "DigitalSignAPIPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public DigitalSignFacade() {
        super(DigitalSign.class);
    }
    
}
